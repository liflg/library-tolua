Website
=======
http://www.codenix.com/~tolua

License
=======
MIT license (see the file source/COPYRIGHT)

Version
=======
1.0.93

Source
======
tolua++-1.0.93.tar.bz2 (sha256: 90df1eeb8354941ca65663dcf28658b67d3aa41daa71133bdd20c35abb1bcaba)

Requires
========
* lua50