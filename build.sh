#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    echo "5): i386-darwin-macos"
    echo "6): x86_64-darwin-macos"
    echo "7): universal-darwin-macos (fat binary i386 and x86_64)"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    elif [ x$SELECTEDOPTION = x"5" ]; then
        MULTIARCHNAME=i386-darwin-macos
    elif [ x$SELECTEDOPTION = x"6" ]; then
        MULTIARCHNAME=x86_64-darwin-macos
    elif [ x$SELECTEDOPTION = x"7" ]; then
        MULTIARCHNAME=universal-darwin-macos
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    rm -rf "$PREFIXDIR"

    ( cd source
      patch -Np2 -i ../patches/tolua++-libs.patch)

    ( LUA50_DIR="${PWD}/../library-lua50/${MULTIARCHNAME}"
      cd source
      scons \
        -j $(nproc) \
        -k \
        prefix="$PREFIXDIR" \
        CCFLAGS="-I"$LUA50_DIR"/include" \
        LINKFLAGS="-L"$LUA50_DIR"/lib"\
        install || true)

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

darwin_build()
{
    echo "FIXME"
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
i386-darwin-macos)
    darwin_build;;
x86_64-darwin-macos)
    darwin_build;;
universal-darwin-macos)
    darwin_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYRIGHT "$PREFIXDIR"/lib/LICENSE-tolua.txt

rm -rf "$BUILDDIR"

echo "tolua for $MULTIARCHNAME is ready."
